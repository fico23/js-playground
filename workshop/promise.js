function getFromDatabase() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({
                id: 1,
                name: 'VOD',
                description: 'bla bla',
                image_id: 35
            })
        }, 100)
    })
}


function getSectionOld() {
    return new Promise((resolve, reject) => {
        getFromDatabase().then((result) => {
            result.otherProperty.a = 123
            resolve(result)
        })
        .catch((error) => {
            reject(error)
        })
    })
}

function getSectionNew() {
    return getFromDatabase().then((result) => {
        result.otherProperty = 123
        return result
    })
}

function prepareRequest(x) {
    x.name = 'basda'
    return getFromDatabase()
}

async function getDataEs6() {
    try {
        const data = await prepareRequest()
        return data
    } catch (e) {
        return {}
    }
};

(async () => {
    getSectionOld()
    .then(x => console.log('old', x))
    .catch((error) => {
        console.log('old error', error)
    })
    
    getSectionNew()
    .then(x => console.log('new', x))
    .catch((error) => {
        console.log('new error', error)
    })
    
    const data = await getDataEs6()
    console.log(data)
})()

