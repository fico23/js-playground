const channels = uniqtv.store.livetv.getAll()
const channelsCount = channels.length

const model = []

// fill channel list
for (let i = 0; i < channelsCount; i++) {
    const channel = channels[i]

    // update qml list view
    model.push({
        position: channel.position,
        name: channel.name
    })
}

// NEW 1
for (const channel of channels)
{
    model.push({
        position: channel.position,
        name: channel.name
    })
}

// NEW 2
const channels = uniqtv.store.livetv.getAll()
const model = channels.map(c => {
    return {
        position: c.position,
        name: c.name
    }
})