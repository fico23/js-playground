const filename = "/home/filip/Documents/newfile"
const fs = require('fs')
var lines = fs.readFileSync(filename, 'utf-8')
    .split('\n')
    .filter(Boolean);

const resultArray = []
const apiCalls = []
let sumApiTime = 0
for (const line of lines)
{
    const time_start = line.indexOf("onDone, time elapsed: ")
    const url_start = line.indexOf("response (truncated): ")
    const response_start = line.indexOf("{")
    const time_end = line.indexOf(' s,', time_start);

    const url = line.substring(url_start + 22, response_start)
    const time = parseFloat(line.substring(time_start + 22, time_end))
    sumApiTime += time
    apiCalls.push(`${url} | ${time}`)

    const response = line.substring(response_start, line.length - 1)

    const existing = resultArray.find(x => x.url === url)
    if (existing)
    {
        existing.count++
        const existingResponse = existing.responses.find(r => r.response === response)
        if (existingResponse)
            existingResponse.count++
        else
            existing.responses.push({
                count: 1,
                response,
                time
            })
    }
    else
    {
        resultArray.push({
            url,
            responses: [{
                count: 1,
                response,
                time
            }],
            count: 1
        })
    }
}

let output = ""
for (let request of resultArray)
{
    output += request.url + "\n"
    output += "count: " + request.count + "\n"
    output += "different responses: " + request.responses.length + "\n"
    const average_response_time = request.responses.map(x => x.time).reduce((prev, next) => prev + next) / request.responses.length
    output += "average response time: " + average_response_time + "\n"
}

fs.writeFileSync('./output1.txt', output);
fs.writeFileSync('./api_calls1.txt', apiCalls.join("\n") + `\n sum time elapsed: ${sumApiTime}`);