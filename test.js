// https://stackoverflow.com/a/41115086

// broken down to for easier understanding

const concat = list => Array.prototype.concat.bind(list)
const promiseConcat = f => x => f().then(concat(x))
const promiseReduce = (acc, x) => acc.then(promiseConcat(x))
const serial = funcs => funcs.reduce(promiseReduce, Promise.resolve([]))

const times = [4, 3, 2, 1]

// next convert each item to a function that returns a promise
const funcs = times.map(time => () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(`waited ${time} seconds`)
            resolve(time)
        }, time * 1000)
    })
})


// execute them serially
serial(funcs)
    .then(console.log.bind(console))