class StoreMyContent {

function runSequentially(promiseArr) {
    return promiseArr.reduce((accum, p) => accum.then(p), Promise.resolve())
}

public processCategory(categoryUid, keyword) {
    return new Promise((resolve, reject) => {
        switch(categoryUid) {
            case "bookmarks-live":
                const item = self.getLiveBookmarkItems(keyword)
                resolve(item)
                break
            case "bookmarks-vod":
                self.getVodBookmarkItems(keyword)
                    .then((vodBookmarks) => {
                        resolve(vodBookmarks)
                    })
                    .catch((error) => reject())
                break
            default:
                console.log('undefined category')
                break
        }
    })
}

ExtensionScriptApis()

public getContentCards(categories: any, keyword: any, sortType: any) {
    log.trace(
        StoreMyContent._tag,
        "getContentCards",
        categories,
        keyword,
        sortType
    )

    const self = this

    return new Promise((resolve, reject) => {
        if (!categories || !categories.length) categories = ["bookmarks-live"]

        const items: any = []

        for (const category of categories)
        {

        }

        const getSingleCategory = (categories: any) => {
            // Save first element of categories array to variable, and remove it form the array
            const categoryUid = categories.shift()
            const goToNext = () => {
                if (categories.length) return getSingleCategory(categories)

                const sortOptions = self.getSortOptions(sortType)
                items.sort(
                    uniqtv.sdk.sortItems(sortOptions.sortKey, sortOptions.reverse)
                )

                resolve(items)
            }

            if (categoryUid === "bookmarks-live") {
                items.push.apply(items, )
                goToNext()
                return
            }

            if (categoryUid === "bookmarks-vod") {
                self
                    .getVodBookmarkItems(keyword)
                    .then((vodBookmarks) => {
                        items.push.apply(items, vodBookmarks)
                        goToNext()
                    })
                    .catch((error) => goToNext())

                return
            }

            goToNext()
        }

        getSingleCategory(categories)
    })
}
}